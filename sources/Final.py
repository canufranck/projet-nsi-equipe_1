# -*- coding: utf-8 -*-

import csv
import time
import json
from tornado import websocket, web, ioloop
from tornado.ioloop import PeriodicCallback
from ev3dev2.sensor import INPUT_1,INPUT_2,INPUT_3, INPUT_4#on importe le module
from ev3dev.ev3 import Sensor#on importe le module

"""

On initialise les 4 capteurs

"""
capteur_temp = Sensor(address=INPUT_1, driver_name='temp')
capteur_angle = Sensor(address=INPUT_2, driver_name='angle')
capteur_pression = Sensor(address=INPUT_3, driver_name='barometric')
capteur_lumin = Sensor(address=INPUT_4, driver_name='light')
capteur_lumin.mode = 'AMBIENT'


BD = []
date=0
date0=time.monotonic()
'''
Les WebSockets permettent une communication bidirectionnelle entre le navigateur et le serveur.

'''

"""
Classe WebSocket avec Tornado
"""
class MonWebSocket(websocket.WebSocketHandler):
  # Sous-classe ou objet pour créer un gestionnaire WebSocket de base.

  
  
  def open(self):
    print ('Connection etablie avec le socket.')
   
    self.callback = PeriodicCallback(self.envoie_donnees, 100)
    self.callback.start()
  
  def on_close(self):
    print ('Connection avec le socket ferme.')
    self.callback.stop()

  
  def check_origin(self, origin):
    return True

  """
  ENVOI DES DONNEES
  """
  def envoie_donnees(self):
    print ("Les donnees sont envoyees")
    date = round(time.monotonic() - date0,1);
    temp = (capteur_temp.value()*10)/100;
    angle = capteur_angle.value();
    pression = round(capteur_pression.value()*4-60,1);
    light = ((capteur_lumin.value())/10)-10;
   
    
    BD.append((date,temp,angle,pression,light))
    donnees = {
    	
	   'x': date,
       'temp':temp,
       'angle':angle,
       'pression':pression,
       'light':light,
       
    }
    
    print (donnees)

    """
    Ecriture dans un ficher csv pour l'historique
    """
    self.write_message(json.dumps(donnees))
    with open('pages/fichier_valeurs.csv','w',newline='') as fichier :

        table = csv.writer(fichier)
        table.writerow(('date','temp','angle','pression','light'))
        table.writerows(BD)
  

    
if __name__ == "__main__":
  #créer une nouvelle application web avec un point de terminaison websocket disponible sur websocket
  
  print ("Demarrage du programme du serveur websocket. En attente des demandes des clients pour ouvrir le websocket ...")
  application = web.Application([(r'/', MonWebSocket),])
#Démarre un serveur pour cette application sur le port donné en gérant les répertoires courants.
#Il s'agit d'un alias pratique pour créer un objet HTTPServer et appeler sa méthode Listen. 
  application.listen(8001)
#Les données (l'écoute) sont fournies sur le port TCP 8001
  ioloop.IOLoop.current().start()
#On démare la boucle d'échange d'E/S dans le socket créé