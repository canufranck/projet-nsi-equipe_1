//Connection au WebSocket

var connection = new WebSocket("ws://192.168.1.4:8001/");

connection.onmessage = function (event) {
  var newData = JSON.parse(event.data);

  //VALEURS DES CAPTEURS
  EA0 = newData.temp;
  EA1 = newData.angle;
  EA2 = newData.pression;
  EA3 = newData.light;
  document.querySelector("#monEA0").innerHTML = EA0;
  document.querySelector("#monEA1").innerHTML = EA1;
  document.querySelector("#monEA2").innerHTML = EA2;
  document.querySelector("#monEA3").innerHTML = EA3;

  // Mettre à jour les valeurs des jauges
  g1.refresh(EA0); // Actualise la première jauge
  g2.refresh(EA1); // Actualise la deuxième jauge
  g3.refresh(EA2); // Actualise la première jauge
  g4.refresh(EA3); // Actualise la deuxième jauge

  // Afficher la météo actuelle
  function afficherImage(valeur) {
    var imageElement = document.getElementById("image");
    var phrase = document.getElementById("phrase");

    // Affichage de la phrase et de l'image dépendament de la pression
    if (valeur > 1051) {
      imageElement.src = "img/sec.png";
      phrase.innerHTML =
        "N'oubliez pas de vous hydrater ! Il fait très chaud !";
    } else if (valeur > 1026 && valeur < 1051) {
      imageElement.src = "img/soleil.png";
      phrase.innerHTML = "Il fait particulièrement beau aujourd'hui";
    } else if (valeur > 1001 && valeur < 1026) {
      imageElement.src = "img/nuageux.png";
      phrase.innerHTML = "Attendez vous à un temps nuageux !";
    } else if (valeur > 981 && valeur < 1001) {
      imageElement.src = "img/pluie.png";
      phrase.innerHTML = "Oubliez pas votre parapluie !";
    } else if (valeur > 800 && valeur < 981) {
      imageElement.src = "img/tempete.png";
      phrase.innerHTML = "Attention tempête !";
    }
  }

  var pression = EA2;
  afficherImage(pression);

  //Direction du vent

  function position(valeur) {
    var pos = document.getElementById("neso");

    if (valeur > 45 && valeur < 135) {
      neso.innerHTML = "Est";
    } else if (valeur > 135 && valeur < 225) {
      neso.innerHTML = "Sud";
    } else if (valeur > 225 && valeur < 315) {
      neso.innerHTML = "Ouest";
    } else if (valeur > 315) {
      neso.innerHTML = "Nord";
    } else if (valeur < 45) {
      neso.innerHTML = "Nord";
    }
  }
  var pos = EA1;
  position(pos);
};

//Paramètres des jauges

var g1 = new JustGage({
  id: "gauge1",
  value: 0,
  min: 0,
  max: 100,
  title: "Température",
  label: "° C",
});

var g2 = new JustGage({
  id: "gauge2",
  title: "Direction du vent",
  label: "label",
  value: 0,
  min: 0,
  max: 365,
  decimals: 0,
  gaugeWidthScale: 0.6,
  pointer: true,
  pointerOptions: {
    toplength: 5,
    bottomlength: 15,
    bottomwidth: 2,
  },
  counter: true,
  donut: true,
  relativeGaugeSize: true,
});

var g3 = new JustGage({
  id: "gauge3",
  value: 0,
  min: 900,
  max: 1100,
  title: "Pression",
  label: "mbar",
});
var g4 = new JustGage({
  id: "gauge4",
  value: 0,
  min: 0,
  max: 100,
  title: "Luminosite",
  label: "%",
});
